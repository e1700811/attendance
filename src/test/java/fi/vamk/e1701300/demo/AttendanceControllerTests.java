package fi.vamk.e1701300.demo;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = { "fi.vamk.e1701300.demo" })
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)
public class AttendanceControllerTests {
	
	@Autowired
	private AttendanceRepository repository;
	
	@Test
	public void postGetDeleteAttendance() {
		//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
		
		Iterable<Attendance> begin = repository.findAll();
	//	System.out.println("Db size in begin: " + IterableUtils.size(begin));
		// given
        Attendance att = new Attendance("RockAndRoll", date);
    //  System.out.println("---LOCAL ATTENDANCE CREATED---");
    //  System.out.println(att.toString());        
        // test save
        Attendance saved = repository.save(att);
    //  System.out.println("---SAVED ATTENDANCE---");
    //  System.out.println(saved.toString());
        // test get
        Attendance found = repository.findByKey(att.getKey());
    //  System.out.println("---FOUND ATTENDANCE---");
    //  System.out.println(found.toString());
        // test delete
        assertThat(found.getKey()).isEqualTo(att.getKey());
        repository.delete(found);
       
        Iterable<Attendance> end = repository.findAll();
    //  System.out.println("Db size in end: " + IterableUtils.size(end));
        assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
		
	}
}