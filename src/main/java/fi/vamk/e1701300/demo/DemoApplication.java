package fi.vamk.e1701300.demo;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DemoApplication {
	
	@Autowired
	private AttendanceRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Bean
	public void initData() {
		Date date = new Date();
		
		Attendance att = new Attendance("ABCDEF", date);
		Attendance att2 = new Attendance("GHIJK", date);
		Attendance att3 = new Attendance("LMNOP", date);
		repository.save(att);
		repository.save(att2);
		repository.save(att3);
	}
}
