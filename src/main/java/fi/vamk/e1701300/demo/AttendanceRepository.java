package fi.vamk.e1701300.demo;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer>{

	public Attendance findByKey(String key);

	public List<Attendance> findAllByDay(Date date);
	
}